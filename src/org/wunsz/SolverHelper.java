package org.wunsz;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * MED�IK OPTIMALIZEJSZYN
 * Created by Bartosz on 2015-04-27.
 */
public class SolverHelper {
    public static File createTempSolverHelper() throws IOException {
        String source =
        "Q1 Q2 Q3 Q4 Q5 Q6 Q7 Q8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "1 2 3 4 5 6 7 8\n" +
        "Q1 Q2 Q3 Q4 Q5 Q6 Q7 Q8 8 rozne\n" +
        "Q1 Q2 - || 1 <>\n" +
        "Q1 Q3 - || 2 <>\n" +
        "Q1 Q4 - || 3 <>\n" +
        "Q1 Q5 - || 4 <>\n" +
        "Q1 Q6 - || 5 <>\n" +
        "Q1 Q7 - || 6 <>\n" +
        "Q1 Q8 - || 7 <>\n" +
        "Q2 Q3 - || 1 <>\n" +
        "Q2 Q4 - || 2 <>\n" +
        "Q2 Q5 - || 3 <>\n" +
        "Q2 Q6 - || 4 <>\n" +
        "Q2 Q7 - || 5 <>\n" +
        "Q2 Q8 - || 6 <>\n" +
        "Q3 Q4 - || 1 <>\n" +
        "Q3 Q5 - || 2 <>\n" +
        "Q3 Q6 - || 3 <>\n" +
        "Q3 Q7 - || 4 <>\n" +
        "Q3 Q8 - || 5 <>\n" +
        "Q4 Q5 - || 1 <>\n" +
        "Q4 Q6 - || 2 <>\n" +
        "Q4 Q7 - || 3 <>\n" +
        "Q4 Q8 - || 4 <>\n" +
        "Q5 Q6 - || 1 <>\n" +
        "Q5 Q7 - || 2 <>\n" +
        "Q5 Q8 - || 3 <>\n" +
        "Q6 Q7 - || 1 <>\n" +
        "Q6 Q8 - || 2 <>\n" +
        "Q7 Q8 - || 1 <>";

        File file = File.createTempFile("tmp-8queens", ".in");
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        bw.write(source);
        bw.close();

        return file;
    }

    public static void deleteTempSolverHelper(File file) throws IOException {
        file.delete();
    }
}
