package org.wunsz.utils;

/**
 * A simple stopwatch implementation for easy way of calculating exeution time
 * @author Bartosz 'Wunsz' Jabłoński
 * @version 1.0
 */
public class Stopwatch {
    private long nanoStartTime;
    private long miliStartTime;

    private long elapsedNanoTime = 0;
    private long elapsedMiliTime = 0;

    /**
     * Begins counting the time (logs current nano and mili time of the system)
     *
     * This method resets the stopwatch!
     */
    public void start(){
        elapsedNanoTime = 0;
        elapsedMiliTime = 0;
        nanoStartTime = System.nanoTime();
        miliStartTime = System.currentTimeMillis();
    }

    /**
     * Stops the stopwatch and updates elapsed time
     */
    public void stop(){
        elapsedNanoTime = getElapsedNanoTime() + System.nanoTime() - nanoStartTime;
        elapsedMiliTime = getElapsedMiliTime() + System.currentTimeMillis() - miliStartTime;
    }

    /**
     * Resumes the count but DOSE NOI reset the stopwatch as start() method
     */
    public void resume(){
        nanoStartTime = System.nanoTime();
        miliStartTime = System.currentTimeMillis();
    }

    /**
     * Standard getter for the elapsed time in nanoseconds
     * @return Elapsed time in nanoseconds
     */
    public long getElapsedNanoTime() {
        return elapsedNanoTime;
    }

    /**
     * Standard getter for the elapsed time in miliseconds
     * @return Elapsed time in miliseconds
     */
    public long getElapsedMiliTime() {
        return elapsedMiliTime;
    }
}
