package org.wunsz.data;

import sun.reflect.generics.tree.Tree;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Created by Bartosz on 2015-04-25.
 */
public class RPNStatement {
    LinkedList<Object> statement;
    public LinkedList<Variable> entangledVariables;
    ArrayList<Object> statementArrayList;
    ArrayList<Variable> entangledVariablesArrayList;
    int varSize;
    int statementArrayLength;
    String statementString;
    public long numberOfEvaluations = 0;

    public RPNStatement(String statementLine, ArrayList<Variable> variables){
        Scanner scanner = new Scanner(statementLine);   // Zczytaj lini� ograniczenia
        Object objectValue = null;
        String stringValue;
        int intValue;

        entangledVariables = new LinkedList<>();
        statement = new LinkedList<>();

        // Do ko�ca linii czytaj kolejne elementy
        while(scanner.hasNext()) {
            stringValue = scanner.next();

            // Je�eli jest intem to jest to sta�a
            // Spr�buj sparsowa� jako int, jak wyjdzie to int jak nie to z�ap wyj�tek
            try {
                intValue = Integer.parseInt(stringValue);
                statement.addLast(intValue);
            } catch (NumberFormatException e) {
                // Pr�bujemy parsowa� operator
                Operator operator = Operator.parseOperator(stringValue);

                // Sprarsowa� i jest operatorem? No to go dodaj i przejd� do kolejnego elementu ograniczenia
                if(operator != null){
                    statement.addLast(operator);

                    continue;
                }

                // A jak nie, to mo�e jest zmiennn�?
                // variables - to wszystkie zmienne, kt�re s� w ca�ym pliku (s� ju� zdefiniowane)
                for (Variable variable : variables) {
                    if (variable.name.equals(stringValue)) {
                        objectValue = variable;                         // Dodaj j� jako obiekt
                        entangledVariables.addLast(variable);           // Do listy zmiennych uwik�anych w ten constraint dodaj t� zmienn�
                        variable.entangledConstraints.addLast(this);    // Do tej zmiennej dodaj, �e jest uwik�ana w aktualny constraint
                        break;
                    }
                }

                // Je�eli znale�li�my zmienn�, to znaczy �e to by�a zmienna
                if(objectValue != null){
                    statement.addLast(objectValue);
                } else { // A jak nie? To to by� zwyk�y string
                    statement.addLast(stringValue);
                }

                objectValue = null;                 // Wyczy�� zmienn�
            }

            statementString = statementLine;        // Zapisz oryginalny statement jako string
        }

        statementArrayList = new ArrayList<>(statement);
        entangledVariablesArrayList = new ArrayList<>(entangledVariables);

        varSize = entangledVariablesArrayList.size();       // Zapisz ile zmiennych jest wpl�tanych
        statementArrayLength = statementArrayList.size();   // Zapisz jak d�ugi jest statement
    }


    /**
     * Wyliczamy warto�� tej dziwki :D
     * @return
     */
    public boolean evaluate() {

        // Je�li kt�ra� zmienna nie jest ustawiona, to za�� �e statement jest TRUE ale nie zwi�kszaj licznika ewaluacji
        for(int i = 0; i < varSize; i++){
            if(entangledVariablesArrayList.get(i).value == null){
                return true;
            }
        }

        numberOfEvaluations++;

       // System.out.print("Checking: " + statementString);
        Stack<Object> stack = new Stack<>();
        int intOne;
        int intTwo;

        // Dla ka�dego elementu wyra�enia
        for(int i = 0; i < statementArrayLength; i++){

            if(statementArrayList.get(i) instanceof Integer || statementArrayList.get(i) instanceof String){
                stack.push(statementArrayList.get(i));                       // Jak to jest sta�a to wrzu� na stos
            } else if (statementArrayList.get(i) instanceof Variable) {
                stack.push(((Variable) statementArrayList.get(i)).value);    // Jak zmienna to we� jej warto�� i wrzu� j� na stos
            } else {
                Operator operator = (Operator) statementArrayList.get(i);    // Jak operator, to we� go i go u�yj na tym co jest na stosie

                switch (operator){
                    case OPERATOR_ADD:                  // Jak to jest dodawanie to we� 2 ostatnie ze stosu i dodaj
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne + intTwo);
                        break;
                    case OPERATOR_SUB:                  // Analogicznie
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne - intTwo);
                        break;
                    case OPERATOR_MUL:                  // Analogicznie
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne * intTwo);
                        break;
                    case OPERATOR_DIV:                  // Analogicznie
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne / intTwo);
                        break;
                    case OPERATOR_EQUAL:                  // Analogicznie
                        stack.push(stack.pop().equals(stack.pop()));
                        break;
                    case OPERATOR_NOTEQ:                  // Analogicznie ( <> )
                        stack.push(!stack.pop().equals(stack.pop()));
                        break;
                    case OPERATOR_LESS:                  // Analogicznie
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne < intTwo);
                        break;
                    case OPERATOR_MORE:                  // Analogicznie
                        intTwo = (int) stack.pop();
                        intOne = (int) stack.pop();
                        stack.push(intOne > intTwo);
                        break;
                    case OPERATOR_ABS:                  // Analogicznie
                        intOne = (int) stack.pop();
                        stack.push(intOne < 0 ? intOne * -1 : intOne);
                        break;
                    case OPERATOR_EXT:                  // Analogicznie
                        int charIndex = (int) stack.pop();
                        stack.push("" + ((String) stack.pop()).charAt(charIndex));
                        break;
                    case OPERATOR_DIF:
                        // Actually this path should be unused
                        // Get the number of variables
                        int numberOfVariables = (int) stack.pop();

                        // Create a set container
                        Set<Object> objectsSet = new HashSet<>();

                        // Read all needed variables and add it to a set.
                        // If we will try to add a value that already exists in the set, we will false from set.add
                        do {
                            numberOfVariables--;
                        } while (numberOfVariables >= 0 && objectsSet.add(stack.pop()));

                        boolean result = numberOfVariables == -1;

                        // Cleanup the mess (if there are any leftovers)
                        while (numberOfVariables > 0) {
                            numberOfVariables--;
                            stack.pop();
                        }

                        // Push result to stack
                        stack.push(result);
                        break;
                    case OPERATOR_SUM:
                        // Actually this path should be unused
                        // Get the number of variables
                        int numberOfVariablesForSum = (int) stack.pop();
                        int initialSum = 0;

                        for(int j = 0; j < numberOfVariablesForSum; j++){
                            initialSum += (int) stack.pop();
                        }

                        // Push result to stack
                        stack.push(initialSum);
                        break;
                }
            }
        }

        return (boolean) stack.pop();
    }

    /**
     * Generalnie najpierw tworz� drzewo wyra�enia
     * Tworz� takie o tszeffko: http://www.profesor.pl/mat/na7/na7_n_wasik_030806_1a.gif
     *
     * Pami�taj! Rozbijamy tylkjo ten najwy�szy wi�c
     * A B C rozne =>
     *  A B <>
     *  A C <>
     *  B C <>
     *
     * Gdzie A B i C to tez mog� by� r�ne, ale ich ju� nie rozbijam. BOm le�.
     * @param statementLine
     * @param variables
     * @return
     */
    public static Collection<? extends RPNStatement> parseDiffConstraint(String statementLine, ArrayList<Variable> variables) {
        TreeNode root = RPNToTree(statementLine);   // <- Tu o je tworze xD I je mam
        root.subNodes.removeLast();                 // Wyjebujemy ostatniego node'a bo to bedzie ten "rozne" a jego nie chcemy

        ArrayList<TreeNode> subNodes = new ArrayList<>(root.subNodes);      // Tworzymy se array liste z subnode'�w
        LinkedList<RPNStatement> statements = new LinkedList<>();           // I przygotowujemy nowy rpn statement (list� ich)

        for(int i = 0; i < subNodes.size(); i++){                           // Lecimy po wszystkich elementach tego "r�ne"
            for(int j = i + 1; j < subNodes.size(); j++){
                String firstStatement = TreeNodeToRPN(subNodes.get(i));     // We� jeden
                String secondStatement = TreeNodeToRPN(subNodes.get(j));    // We� drugi
                // I stw�rz z nich nowy string rpn czyli np A B <> i to sparsuj jak normalny
                statements.addLast(new RPNStatement(firstStatement + " " + secondStatement + " " + Operator.STR_OPERATOR_NOTEQ, variables));
            }
        }

        return statements;
    }

    private static TreeNode RPNToTree(String statementLine){
        Scanner scanner = new Scanner(statementLine);   // Zczytaj statment
        String stringValue;

        Stack<TreeNode> stack = new Stack<>();

        while(scanner.hasNext()) {                                      // Czytaj kolejne elementy
            stringValue = scanner.next();                               // Wrzucamy do zmiennej
            Operator operator = Operator.parseOperator(stringValue);    // Pr�ba sparsowania do operatora

            TreeNode node = new TreeNode(stringValue);                  // Stw�rz nowy node w drzewie REKURSJA DZIFFKO!

            if(operator != null) {                                      // Jezeli to jest operator
                while(!stack.empty()){                                  // To dopuki stos nie jest pusty
                    node.subNodes.addFirst(stack.pop());                // Dorzucaj kolejne elementy ze stosu jako kolejne ga��zie drzewa
                }
            }

            stack.push(node);                                           // Potem wrzu� ca�ego node'a na stos
        }

        if (stack.size() != 1) {                                        // Je�eli po tym cyrku stos jest r�ny od 1 to co� sie zjeba�o
            throw new RuntimeException("Expected exactly one stack value.");
        }

        return stack.pop();                                             // Zwr�� tego jedynego node'a
    }

    private static String TreeNodeToRPN(TreeNode node){
        StringBuilder statement = new StringBuilder();

        for(TreeNode subNode : node.subNodes){
            statement.append(TreeNodeToRPN(subNode));
        }

        statement.append(node.value);

        return statement.toString();
    }
}

class TreeNode {
    LinkedList<TreeNode> subNodes;
    String value;

    public TreeNode(String value){
        this.value = value;
        subNodes = new LinkedList<>();
    }
}
