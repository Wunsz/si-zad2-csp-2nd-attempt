package org.wunsz.data;

/**
 * Created by Bartosz on 2015-04-25.
 */
public enum Operator {
    OPERATOR_ADD,
    OPERATOR_SUB,
    OPERATOR_MUL,
    OPERATOR_DIV,
    OPERATOR_EQUAL,
    OPERATOR_NOTEQ,
    OPERATOR_LESS,
    OPERATOR_MORE,
    OPERATOR_ABS,
    OPERATOR_EXT,
    OPERATOR_DIF,
    OPERATOR_SUM;

    // Calculations - Numbers only
    protected static final String STR_OPERATOR_ADD = "+";
    protected static final String STR_OPERATOR_SUB = "-";
    protected static final String STR_OPERATOR_MUL = "*";
    protected static final String STR_OPERATOR_DIV = "/";

    // Comparisons - All types
    protected static final String STR_OPERATOR_EQUAL = "=";
    protected static final String STR_OPERATOR_NOTEQ = "<>";

    // Order comparisons - number only
    protected static final String STR_OPERATOR_LESS = "<";
    protected static final String STR_OPERATOR_MORE = ">";

    // Absolute value of given argument - number only
    protected static final String STR_OPERATOR_ABS = "||";

    // Extraction - array operator "Test" 0 [] will get you T - String only
    protected static final String STR_OPERATOR_EXT = "[]";

    // All different - a b c 3 rozne
    public static final String STR_OPERATOR_DIF = "rozne";

    // Sum operator
    public static final String STR_OPERATOR_SUM = "suma";

    /**
     * Based on given string returs a proper enum value or null if it doesn't exist.
     *
     * @param name
     * @return
     */
    public static Operator parseOperator(String name) {
        switch (name) {
            case STR_OPERATOR_ADD:
                return Operator.OPERATOR_ADD;

            case STR_OPERATOR_SUB:
                return Operator.OPERATOR_SUB;

            case STR_OPERATOR_MUL:
                return Operator.OPERATOR_MUL;

            case STR_OPERATOR_DIV:
                return Operator.OPERATOR_DIV;

            case STR_OPERATOR_EQUAL:
                return Operator.OPERATOR_EQUAL;

            case STR_OPERATOR_NOTEQ:
                return Operator.OPERATOR_NOTEQ;

            case STR_OPERATOR_LESS:
                return Operator.OPERATOR_LESS;

            case STR_OPERATOR_MORE:
                return Operator.OPERATOR_MORE;

            case STR_OPERATOR_ABS:
                return Operator.OPERATOR_ABS;

            case STR_OPERATOR_EXT:
                return Operator.OPERATOR_EXT;

            case STR_OPERATOR_DIF:
                return Operator.OPERATOR_DIF;

            case STR_OPERATOR_SUM:
                return Operator.OPERATOR_SUM;

        }

        return null;
    }
}

