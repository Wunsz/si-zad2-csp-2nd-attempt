package org.wunsz.data;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Bartosz on 2015-04-25.
 */
public class Variable {
    public LinkedList<Object> domain;
    public LinkedList<RPNStatement> entangledConstraints;
    public HashMap<Variable, LinkedList<RPNStatement>> entangledVariables;
    public Object value;
    public String name;

    public Variable(String name){
        this.name = name;
        domain = new LinkedList<>();
        entangledConstraints = new LinkedList<>();
    }

    public static Object getNativeValue(String value){
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return value;
        }
    }
}
