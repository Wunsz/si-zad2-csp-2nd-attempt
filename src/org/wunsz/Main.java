package org.wunsz;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;

public class Main {

    public static void main(String[] args) throws IOException {
        // Zczytywanie danych z konsoli/opcji startowych
        String filePath = args.length > 0 ? args[0] : "D:\\Dropbox\\Politechnika\\6 Semestr\\SI\\ProblemGenerator\\NQueens-12.in";  // Sciezka do pliku z zadaniem
        Boolean findFirst = args.length > 1 ? args[1].equals("first") : false;
        Boolean fc = args.length > 2 ? args[2].equals("fc") : false;
        Boolean bt = args.length > 2 ? args[2].equals("bt") : false;
        Boolean all = args.length < 2 || args.length > 2 && !args[2].equals("bt") && !args[2].equals("fc");
        Boolean saveToFile = args.length > 3 ? args[3].equals("save") : false;

        MainSolver solver;

        System.out.println("Preparing...");
        // This somehow speeds up calculations... But lets mute it.

        // Tutaj dzieje si� magia
        /**
         * Generalnie zamykam output �eby nie sra� na ekran. Bo magia tajna.
         * Tworz� nowy tymczasowy plik, wczytuj� go (8 Hetman�w), rozwi�zuje problem
         * Boom zapominam o tym. I magicznie to przyspiesza dalsze kalkulacje.
         *
         * Mo�e to przez tworzenie jaki� obiekt�w albo inne D�AWOWE/D�AJOWE keszowanie
         */
        PrintStream realSystemOut = System.out;
        System.setOut(new PrintStream(new NullOutputStream()));

        File tempFile = SolverHelper.createTempSolverHelper();
        solver = new MainSolver(tempFile);
        solver.setSaveResults(false);
        solver.setFindFirst(false);
        solver.solveByBacktracking();
        SolverHelper.deleteTempSolverHelper(tempFile);

        System.setOut(realSystemOut);
        // Tutaj magia si� ko�czy


        solver = new MainSolver(new File(filePath));    // Utw�rz obiekt solwera
        solver.setFindFirst(findFirst);                 // Ustaw czy szukamy wszystkich czy tylko pierwszego
        solver.setSaveResults(saveToFile);              // Ustaw czy zapisujemy do pliku czy nie

        if(bt || all){
            solver.solveByBacktracking();
        }

        System.out.println();

        if(fc || all){
            solver.solveByForwardChecking();
        }
    }

    private static class NullOutputStream extends OutputStream {

        @Override
        public void write(int b) throws IOException {
            return;
        }
    }
}
