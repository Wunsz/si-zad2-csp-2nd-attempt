package org.wunsz;

import com.sun.org.apache.xpath.internal.SourceTree;
import org.wunsz.data.Operator;
import org.wunsz.data.RPNStatement;
import org.wunsz.data.Variable;
import org.wunsz.solvers.Backtracking;
import org.wunsz.solvers.ForwardChecking;
import org.wunsz.utils.Stopwatch;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by Bartosz on 2015-04-26.
 */
public class MainSolver {
    ArrayList<Variable> variables;
    ArrayList<RPNStatement> constraints;
    File file;
    private Boolean findFirst = false;
    private Boolean saveResults = true;

    /**
     * Creates the solve instance
     *
     * @param file
     */
    public MainSolver(File file) {
        this.file = file;       // Przypisz plik do zmiennej
        readFile(file);         // Wczytaj go
        preparePairs();         // Przygotuj pary???
    }

    /**
     * Prepares a HashMap of all variables that are entangled with each variable with a list of constratis that
     * tie them in.
     */
    private void preparePairs() {
        HashMap<Variable, LinkedList<RPNStatement>> pairs;

        for(Variable firstVariable : variables){
            pairs = new HashMap<>();

            for(Variable secondVariable : variables){
                LinkedList<RPNStatement> firstEntangledConstraints = (LinkedList<RPNStatement>) firstVariable.entangledConstraints.clone();
                firstEntangledConstraints.removeAll(secondVariable.entangledConstraints);

                if(firstEntangledConstraints.size() > 0){
                    pairs.put(secondVariable, firstEntangledConstraints);
                }
            }

            firstVariable.entangledVariables = pairs;
        }


    }


    /**
     * Attempts to solve the given problem by Backtracking algorithm
     */
    public void solveByBacktracking() {
        System.out.printf("Attempting to solve %s by Backtracking\n", file.getName());
        System.out.printf("Stop after first: %b\n", findFirst);
        Stopwatch stopwatch = new Stopwatch();

        for(RPNStatement statement : constraints){
            statement.numberOfEvaluations = 0;
        }

        Backtracking backtracking = new Backtracking(variables);
        backtracking.setFinishAfterFirst(findFirst);

        stopwatch.start();
        backtracking.solve();
        stopwatch.stop();

        long totalEvaluations = 0;

        for(RPNStatement statement : constraints){
            totalEvaluations += statement.numberOfEvaluations;
        }

        System.out.printf("\nNumber of results: %d\n", backtracking.getNumberOfResults());
        System.out.printf("Time taken: %d ms\n", stopwatch.getElapsedMiliTime());
        System.out.println("Number of evaluations: " + totalEvaluations);

        if(saveResults) {
            String filename = file.getName() + ".BT.out";
            logResultToFile(backtracking.getResultsAsString(), filename);
            System.out.println("Results saved to: " + filename);
        }

        System.out.println();
    }

    /**
     * Attempts to solve the given problem by Backtracking algorithm
     */
    public void solveByForwardChecking() {
        System.out.printf("Attempting to solve %s by Forward Checking\n", file.getName());
        System.out.printf("Stop after first: %b\n", findFirst);
        Stopwatch stopwatch = new Stopwatch();

        for(RPNStatement statement : constraints){
            statement.numberOfEvaluations = 0;
        }

        ForwardChecking forwardChecking = new ForwardChecking(variables);
        forwardChecking.setFinishAfterFirst(findFirst);

        stopwatch.start();
        forwardChecking.solve();
        stopwatch.stop();

        long totalEvaluations = 0;

        for(RPNStatement statement : constraints){
            totalEvaluations += statement.numberOfEvaluations;
        }

        System.out.printf("\nNumber of results: %d\n", forwardChecking.getNumberOfResults());
        System.out.printf("Time taken: %d ms\n", stopwatch.getElapsedMiliTime());
        System.out.println("Number of evaluations: " + totalEvaluations);

        if(saveResults) {
            String filename = file.getName() + ".FC.out";
            logResultToFile(forwardChecking.getResultsAsString(), filename);
            System.out.println("Results saved to: " + filename);
        }


        System.out.println();
    }



    /**
     * Reads the problem file
     *
     * @param file File with the problem definition
     */
    private void readFile(File file) {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            variables = new ArrayList<>();
            constraints = new ArrayList<>();

            // Reading variablesWithDomains
            Scanner scanner = new Scanner(br.readLine());           // Czytamy 1 lini� pliku (Lista zmiennych)
            while (scanner.hasNext()) {
                variables.add(new Variable(scanner.next()));        // Czytamy po spacji kolejne zmienne
            }

            int numberOfVariables = variables.size();               // Liczymy ile jest zmiennych bo tyle bedzie kolejnych linii definiuj�cych dziedziny

            // Reading variables domains
            for (int i = 0; i < numberOfVariables; i++) {
                scanner = new Scanner(br.readLine());               // Czytamy po linijce

                while (scanner.hasNext()) {                         // Do ko�ca po spacji warto�ci
                    variables.get(i).domain.addLast(Variable.getNativeValue(scanner.next()));
                }
            }

            // Reading constraints (till the end of the file)       // Do ko�ca pliku, ka�da linia to constraint
            for (String constraintLine; (constraintLine = br.readLine()) != null; ) {
               if(constraintLine.endsWith(Operator.STR_OPERATOR_DIF)){                                  // Kolejna magia optymalizacji, ograniczenie "rozne" traktujemy osobno
                   constraints.addAll(RPNStatement.parseDiffConstraint(constraintLine, variables));     // Szczeg�y w metodzie
                }

                constraints.add(new RPNStatement(constraintLine, variables));                           // Je�li nie to traktuj normalnie
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setFindFirst(Boolean findFirst) {
        this.findFirst = findFirst;
    }

    public Boolean getFindFirst() {
        return findFirst;
    }

    /**
     * Hurr durr zapisyuwanie do pliku
     * @param result
     * @param filename
     */
    private void logResultToFile(String result, String filename){
        PrintWriter writer = null;
        try {
            writer = new PrintWriter(filename, "UTF-8");
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        writer.print(result);
        writer.close();
    }

    public Boolean getSaveResults() {
        return saveResults;
    }

    public void setSaveResults(Boolean saveResults) {
        this.saveResults = saveResults;
    }
}
