package org.wunsz.solvers;

import org.wunsz.data.Variable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public abstract class AbstractSolver implements ISolver {
    protected ArrayList<Variable> variables;
    protected int numberOfVariables;
    protected int numberOfResults;

    /**
     * To jest lista wynik�w, a ka�dy wynik to s�ownik (HashMap): nazwa zmiennej => warto��
     */
    protected LinkedList<HashMap<String, Object>> results;
    protected boolean finishAfterFirst;

    @Override
    public void solve() {
        numberOfResults = 0;                    // Zerujemy liczbe wynik�w
        numberOfVariables = variables.size();   // Ustawiamy ile jest zmiennych (ile bedzie leweli)
        results = new LinkedList<>();           // Czy�cimy list� wynik�w
        try{
            next(0);                            // Rozpoczynamy nakurwianie Backtrackingu lub ForwardCheckingu
        } catch (FirstValueFoundException e){}  // �apiemy wyj�tek o pierwszym rozwi�zaniu

    }

    @Override
    public int getNumberOfResults() {
        return numberOfResults;
    }

    /**
     * Lista wynik�w (linkedList)
     *      W kt�rej wynik jest hashMap�
     *          W kt�rej:
     *              Klucz -> nazwa zmiennej
     *              Warto�� -> warto�� zmiennej
     * @return
     */
    @Override
    public LinkedList<HashMap<String, Object>> getResults() {
        return results;
    }

    /**
     * BUdujemy wszystkie wyniki jako tabelk� np:
     * A B C D E
     * 1 2 3 4 5
     * 2 3 1 5 2
     * 1 4 5 2 2
     *
     * @return
     */
    @Override
    public String getResultsAsString(){
        StringBuilder builder = new StringBuilder();

        // Budujemy nag��wek (lista zmiennych odzielonych spacjami
        for(String header : results.getFirst().keySet()){
            builder.append(header);
            builder.append(" ");
        }

        // Dla ka�dej hashmapy w li�cie wynik�w
        for(HashMap<String, Object> values : results){
            // Wypisz znak nowej linii
            builder.append("\n");

            // Dla ka�dej warto�ci w hashmapie
            for(Object value : values.values()){
                // Wydrukuj warto��
                builder.append(value);
                builder.append(" ");
            }
        }

        return builder.toString();
    }

    protected void reportProgress(int level){
        if(level == 1){
            System.out.print(".");
        }
    }

    /**
     * Zapisuje snapshot wszystkich zmiennych z aktualnie przypisanymi warto�ciami
     * Do zapisywania wynik�w!
     * @return
     */
    protected HashMap<String, Object> getVariablesSnapshot() {
        // Swt�rz hashmape typu nazwaZmiennej => warto��
        HashMap<String, Object> snapshot = new HashMap<>();

        // Dla ka�dej zmiennej
        for(Variable variable : variables){
            // Wstaw j� do hashmapy  jako nazwa => warto��
            snapshot.put(variable.name, variable.value);
        }

        // Zwr�� ca�y snapszot
        return snapshot;
    }

    // To tu jest zeby trza by�o zaimplementowa� w dziedzicz�cych klasach
    protected abstract void next(int level) throws FirstValueFoundException;

    /**
     * Zwraca warto�� poni�ej opisanej flagi
     * @return
     */
    public boolean isFinishAfterFirst() {
        return finishAfterFirst;
    }

    /**
     * Ustawia czy powinni�my sko�czy� po pierwszym, czy nie
     * @param finishAfterFirst
     */
    public void setFinishAfterFirst(boolean finishAfterFirst) {
        this.finishAfterFirst = finishAfterFirst;
    }

    /**
     * Eksepszyn kt�ry �apiemy by wyj�� z liczenia po pierwszej znalezionej warto�ci.
     */
    protected class FirstValueFoundException extends Exception {

    }
}
