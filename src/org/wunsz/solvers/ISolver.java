package org.wunsz.solvers;

import java.util.HashMap;
import java.util.LinkedList;

/**
 * Created by Bartosz on 2015-04-26.
 */
public interface ISolver {
    public void solve();
    public int getNumberOfResults();
    public LinkedList<HashMap<String, Object>> getResults();
    public String getResultsAsString();
}
