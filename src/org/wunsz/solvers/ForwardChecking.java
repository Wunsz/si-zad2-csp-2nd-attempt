package org.wunsz.solvers;

import org.wunsz.data.RPNStatement;
import org.wunsz.data.Variable;

import java.util.*;

public class ForwardChecking extends AbstractSolver implements ISolver {

    public ForwardChecking(ArrayList<Variable> variables){
        this.variables = variables;
    }

    /**
     * Rekursywna metoda kalkuluj�ca warto�ci zmiennych na kolejnych poziomach
     * Level to tak naprawd� jest numer zmiennej
     * @param level
     */
    protected void next(int level) throws FirstValueFoundException{
        reportProgress(level);                              // Druknij kropk� (przygarnij kropka)

        Variable currentVariable = variables.get(level);    // Odczytaj zmienn� z listy zmiennych dla danego poziomu
                                                            // Java szybciej odwo�uje sie do zmiennych lokalnych

        for(Object value : currentVariable.domain){         // Dla ka�dej warto�ci z dziedziny danej zmiennej

            currentVariable.value = value;                  // Przypisz zmiennej t� warto��

            // Ogranicz dziedziny innych zmiennych (tj. wywal wszystkie warto�ci z dziedzin innych zmiennych, kt�re nie spe�niaj� ogranicze�)
            // Bierzemy pod uwag� tylko te ograniczenia, w kt�rych wyst�puje aktualna zmienna (currentVariable)
            // Jako wynik dostajemy backup warto��i przed zmniejszaniem do kt�rego b�dziemy potem wraca�
            HashMap<Variable, LinkedList<Object>> domainBackup = shrinkDomains(currentVariable);

            // Je�li jeste�my na ostatnim poziomie (Nadali�my warto�� ostatniej zmiennej), to sprwawdzamy ograniczenia
            if(level == numberOfVariables - 1){
                if(checkConstrains(currentVariable)){   //Jak s� ok to mamy wynik
                    numberOfResults++;                          // Zwi�kszamy o 1 ilo�� znalezionych rozwi�za�
                    results.addLast(getVariablesSnapshot());    // Dodajemy rozwi�zanie (snapszot warto�ci) do listy rozwi�za�

                    if(finishAfterFirst){                       // Jak to mia� by� tylko 1 wynik to wy�azimy przez exception
                        throw new FirstValueFoundException();
                    }
                }
            } else {                                            // No a jak to nie jest ostatni lewel to lecim dalej
                next(level + 1);
            }

            // Jak sko�czyli�my dzia�a� na tym pozimie (wszystkie zmienne maj� warto��, to odtwarzamy
            // dziedziny zmiennych (do tego stanu przed shrinkDomains)
            restoreDomains(domainBackup);

            // Zerujemy aktualn� zmienn�
            currentVariable.value = null;
        }
    }

    /**
     * Odtwarza warto�ci zmiennych z zapisanego backupu
     *
     * Dlaczego set?
     * W javie HashMap nie ma zaimplementowanej obs�ugi ForEach (For(int i : arrayOfInts))
     *
     * A wi�c robimy to manualnie:
     *      1. Pobierz wszystkie klucze z hashmapy
     *      2. Dla ka�dej warto�ci klucza:
     *          a) Oczytaj warto��, kt�ra jest przypisana do tego klucza
     *          b) Zapisz t� warto�� jako dziedzin� zmiennej
     *
     * Zauwa�, �e kluczem hashmapy jest zmienna (�e obiekt klasy Variable)! Wi�c odrazu mamy do niej dost�p :)
     *
     * @param domainBackup
     */
    private void restoreDomains(HashMap<Variable, LinkedList<Object>> domainBackup) {
        Set<Variable> variables = domainBackup.keySet();        // Jako �e klucze w s�owniku (hashmap) nie mog� si� powtarza�, to bierzemy z nich zbi�r (set)
        for(Variable variable : variables){                     // Dla ka�dej zmiennej w tym s�owniku
            variable.domain = domainBackup.get(variable);       // Odczytaj list� dziedzin� i przypisz j� zmiennej
        }
    }

    /**
     * Zmniejsza dziedziny zmiennych, kt�re s� zwi�zane z aktualn� zmienn� poprzez ograniczenia,
     * ale tylko tych, kt�re nie maj� jeszcze nadanej warto�ci.
     *
     * @param variable
     * @return Snapszot warto�ci domen przez zmniejszaniem
     */
    protected HashMap<Variable, LinkedList<Object>> shrinkDomains(Variable variable){
        // Przygotuj zmienn� do czego�
        Object value;

        // Przygotuj HashMap� do backupu
        HashMap<Variable, LinkedList<Object>> backup = new HashMap<>();

        // Dla ka�dego ograniczenia, w kt�rym wyst�puje aktualna zmienna
        for(RPNStatement statement : variable.entangledConstraints){

            // Dla ka�dej zmiennej wyst�puj�cej w tym ograniczeniu
            for(Variable entangledVariable : statement.entangledVariables){

                // Je�li nie ma przyznanej warto�ci
                if(entangledVariable.value == null){

                    // Je�eli nie ma backupu dla tej zmiennej to trzea go stworzy� i zapisa�
                    if(!backup.containsKey(entangledVariable)){
                        // Do haszmapy wsad� dziedzin�:
                        // Klucz -> obiekt zmiennej dla kt�rej robimy backupa
                        // Warto�� -> Klon dziedziny tej zmiennej (Klon bo b�dziemy potem zmniejsza� t� dziedzine wiec referencja nie wystarczy)
                        backup.put(entangledVariable, (LinkedList<Object>) entangledVariable.domain.clone());
                    }

                    // Dla ka�dej warto�ci w dziedzinie zmiennej (tej backupowanej) sprawd� ograniczenia
                    Iterator iterator = entangledVariable.domain.iterator();    // Wytargaj iterator
                    while(iterator.hasNext()) {                                 // Dopuki jest kolejna zmienna
                        value = iterator.next();                                // Przypisz j� do lokajnej warto�ci
                        entangledVariable.value = value;                        // Przypisz zmiennej t� warto��

                        // Je�eli ten aktualny statement nie jest spe�niony
                        if (!statement.evaluate()) {
                            iterator.remove();          // To wyjeb t� zmienn� z dziedziny
                        }
                    }

                    // Wyczy�� warto�� zmiennej (TUTAJ TO JEST W CHUJ WA�NE!)
                    entangledVariable.value = null;
                }
            }
        }

        // Zwr�� obiekt backupy
        return backup;
    }

    /**
     * Sprwad� czy aktualne warto�ci zmiennych spe�niaj� wszystkie ograniczenia,
     * w kt�rych wyst�puje aktualna zmienna
     *
     * @param variable
     * @return true je�eli wsio spe�nione lub niemo�liwe do wyliczenia bo brakuje warto�ci zmiennej
     */
    protected boolean checkConstrains(Variable variable) {
        for(RPNStatement statement : variable.entangledConstraints){    // Dla ka�dego ograniczenia w ograniczeniach powi�zanych z dan� zmienn� (Zmienna w nich wyst�puje)
            if(!statement.evaluate()) {                                 // Je�eli ograniczenie zwr�ci false, to zwr�� false (przerwij p�tl� bo nie ma co dalej sprawdza�)
                return false;
            }
        }

        return true;
    }

    /**
     * Do debugowania
     */
    private void printVariablesWithValues(){
        for(Variable variable : variables){
            if(variable.value != null) {
                System.out.printf(variable.name + "=" + variable.value + " ");
            }
        }
        System.out.println();
    }
}
