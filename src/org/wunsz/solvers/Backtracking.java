package org.wunsz.solvers;

import org.wunsz.data.RPNStatement;
import org.wunsz.data.Variable;

import java.util.ArrayList;

public class Backtracking extends AbstractSolver implements ISolver {

    public Backtracking(ArrayList<Variable> variables){
        this.variables = variables;
    }

    /**
     * Rekursywna metoda kalkuluj�ca warto�ci zmiennych na kolejnych poziomach
     * Level to tak naprawd� jest numer zmiennej
     * @param level
     */
    protected void next(int level) throws FirstValueFoundException {
        reportProgress(level);                              // Wpluwamy kropk� na 1 levelu jak nie to nic

        Variable currentVariable = variables.get(level);    // Odczytaj zmienn� z listy zmiennych dla danego poziomu
                                                            // Java szybciej odwo�uje sie do zmiennych lokalnych

        // Dla ka�dej warto�ci z dziedziny aktualnej zmiennej:
        //      1. Przypisz warto�� do zmiennej
        //      2. Sprawd� ograniczenia
        //      3. Je�li to mo�liwe to przejd� do kolejnej zmiennej
        for(Object value : currentVariable.domain){
            currentVariable.value = value;                  // Przypisanie warto�ci do zmiennej

            if(checkConstrains(currentVariable)){           // Sprwadzenie ogranicze�
                if(level == numberOfVariables - 1){         // Je�eli to jest ostatni level to:
                    numberOfResults++;                          // Zwi�ksz ilo�� poprawnych rozwi�za�
                    results.addLast(getVariablesSnapshot());    // Zr�b snapszot i zapisz go do wynik�w

                    if(finishAfterFirst){                       // Je�eli mamy sie zatrzyma� po 1 to:
                        throw new FirstValueFoundException();   // Jebnij eksepszyn bo tak jest pro�ciej xD
                    }

                } else {                                     // A je�li nie to idziemy lewel ni�ej (nast�pna zmienna)
                    next(level + 1);                            // O jeb rekursja xD
                }
            }

            currentVariable.value = null;                    // Wyczy�� warto�� zmiennej (w sumie dla estetyki raczej)
        }
    }

    /**
     * Sprwad� czy aktualne warto�ci zmiennych spe�niaj� wszystkie ograniczenia,
     * w kt�rych wyst�puje aktualna zmienna
     *
     * @param variable
     * @return true je�eli wsio spe�nione lub niemo�liwe do wyliczenia bo brakuje warto�ci zmiennej
     */
    protected boolean checkConstrains(Variable variable) {
        for(RPNStatement statement : variable.entangledConstraints){    // Dla ka�dego ograniczenia w ograniczeniach powi�zanych z dan� zmienn� (Zmienna w nich wyst�puje)
            if(!statement.evaluate()) {                                 // Je�eli ograniczenie zwr�ci false, to zwr�� false (przerwij p�tl� bo nie ma co dalej sprawdza�)
                return false;
            }
        }

        return true;
    }
}
